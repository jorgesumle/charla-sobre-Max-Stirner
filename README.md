## Charla sobre Max Stirner

Material de apoyo para charla sobre Max Stirner.

Incluye una prueba para comprobar si se han adquirido conocimientos en
el archivo `prueba.html` ([demostración](https://freakspot.net/temporal/charla-sobre-Max-Stirner/prueba.html)).
